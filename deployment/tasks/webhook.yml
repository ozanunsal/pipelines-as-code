---
- name: A-Team Pipeline | webhook | TriggerTemplate
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: triggers.tekton.dev/v1alpha1
      kind: TriggerTemplate
      metadata:
        name: a-team
        namespace: "{{ ocp_ns }}"
      spec:
        params:
          - name: GitRepoUrl
            description: The git repository url
          - name: GitRevision
            description: The git revision
            default: main
          - name: GitMergeIid
            description: The Merge Request id
          - name: GitSourceProjectId
            description: The source project id
          - name: GitTargetRepoUrl
            description: The target git repository url
          - name: GitTargetProjectId
            description: The target project id
          - name: GitReleaseTag
            description: the version for which the repository should be built, triggered by the release hook
            default: ""
          - name: GitCommitBefore
            description: The commit before MR is merged
          - name: GitCommitAfter
            description: The latest commit merged in default branch
        resourcetemplates:
          - apiVersion: tekton.dev/v1beta1
            kind: PipelineRun
            metadata:
              generateName: a-team-
              labels:
                a-team/merge_request_id: $(tt.params.GitMergeIid)
            spec:
              serviceAccountName: pipeline
              timeout: 2h0m0s
              workspaces:
                - name: shared-workspace
                  volumeClaimTemplate:
                    spec:
                      accessModes:
                        - ReadWriteOnce
                      storageClassName: gp2-csi
                      resources:
                        requests:
                          storage: 1.5Gi
                - name: ssl-ca-directory
                  secret:
                    secretName: ca-certs
              pipelineRef:
                name: a-team
              params: "{{ params_hook }}"

- name: A-Team Pipeline | webhook | Product Build TriggerTemplate
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: triggers.tekton.dev/v1alpha1
      kind: TriggerTemplate
      metadata:
        name: product-build
        namespace: "{{ ocp_ns }}"
      spec:
        params:
          - name: GitRepoUrl
            description: The git repository url
          - name: GitRevision
            description: The git revision
            default: main
          - name: GitMergeIid
            description: The Merge Request id
          - name: GitSourceProjectId
            description: The source project id
          - name: GitTargetRepoUrl
            description: The target git repository url
          - name: GitTargetProjectId
            description: The target project id
          - name: GitReleaseTag
            description: the version for which the repository should be built, triggered by the release hook
            default: ""
          - name: GitCommitBefore
            description: The commit before MR is merged
          - name: GitCommitAfter
            description: The latest commit merged in default branch
        resourcetemplates:
          - apiVersion: tekton.dev/v1beta1
            kind: PipelineRun
            metadata:
              generateName: product-build-
              labels:
                toolchain/product-build: $(tt.params.GitRevision)
            spec:
              serviceAccountName: pipeline
              timeout: 2h0m0s
              workspaces:
                - name: shared-workspace
                  volumeClaimTemplate:
                    spec:
                      accessModes:
                        - ReadWriteOnce
                      storageClassName: gp2-csi
                      resources:
                        requests:
                          storage: 1Gi
                - name: ssl-ca-directory
                  secret:
                    secretName: ca-certs
              pipelineRef:
                name: product-build
              params: "{{ params_hook }}"

- name: A-Team Pipeline | webhook | TriggerBinding
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: triggers.tekton.dev/v1alpha1
      kind: TriggerBinding
      metadata:
        name: a-team
        namespace: "{{ ocp_ns }}"
      spec:
        params:
          - name: GitRepoUrl
            value: $(body.object_attributes.source.git_http_url)
          - name: GitTargetRepoUrl
            value: $(body.object_attributes.target.git_http_url)
          - name: GitRevision
            value: $(body.object_attributes.last_commit.id)
          - name: GitMergeIid
            value: $(body.object_attributes.iid)
          - name: GitSourceProjectId
            value: $(body.object_attributes.source.id)
          - name: GitTargetProjectId
            value: $(body.object_attributes.target.id)

- name: A-Team Pipeline | webhook | Product Build TriggerBinding
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: triggers.tekton.dev/v1alpha1
      kind: TriggerBinding
      metadata:
        name: product-build
        namespace: "{{ ocp_ns }}"
      spec:
        params:
          - name: GitRepoUrl
            value: $(body.project.git_http_url)
          - name: GitRevision
            value: main
          - name: GitReleaseTag
            value: $(extensions.GitReleaseTag)
          - name: GitSourceProjectId
            value: $(body.project.id)
          - name: GitCommitBefore
            value: $(body.before)
          - name: GitCommitAfter
            value: $(body.after)

- name: A-Team Pipeline | webhook | Trigger
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: triggers.tekton.dev/v1alpha1
      kind: Trigger
      metadata:
        name: a-team
        namespace: "{{ ocp_ns }}"
      spec:
        serviceAccountName: pipeline
        interceptors:
          - ref:
              name: cel
            params:
              - name: filter
                value: |
                  header.match('X-Gitlab-Event', 'Merge Request Hook') &&
                  !body.object_attributes.title.contains("WIP:") &&
                    (
                      body.object_attributes.action == "open" ||
                      body.object_attributes.action == "reopen" ||
                      (has(body.changes.title) && body.changes.title.previous.contains("WIP:")) ||
                      (has(body.changes.title) && body.changes.title.previous.contains("Draft:")) ||
                      (body.object_attributes.action == "update" && has(body.object_attributes.oldrev))
                    )
              - name: overlays
                value:
                  - key: project_id
                    expression: body.project.id
                  - key: merge_request_id
                    expression: body.object_attributes.id
                  - key: url
                    expression: body.project.git_ssh_url
                  - key: ref
                    expression: body.object_attributes.last_commit.id
        bindings:
          - ref: a-team
        template:
          ref: a-team

- name: A-Team Pipeline | webhook | Product Build Trigger
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: triggers.tekton.dev/v1alpha1
      kind: Trigger
      metadata:
        name: product-build
        namespace: "{{ ocp_ns }}"
      spec:
        serviceAccountName: pipeline
        interceptors:
          - ref:
              name: cel
            params:
              - name: filter
                value: |
                  header.match('X-Gitlab-Event', 'Push Hook') ||
                  (header.match('X-Gitlab-Event','Tag Push Hook') &&
                  body.after != "0000000000000000000000000000000000000000")
              - name: overlays
                value:
                  - key: project_id
                    expression: body.project.id
                  - key: url
                    expression: body.project.git_ssh_url
                  - key: GitReleaseTag
                    expression: "body.ref.split('/')[2]"
                  - key: commit_before
                    expression: body.before
                  - key: commit_after
                    expression: body.after
        bindings:
          - ref: product-build
        template:
          ref: product-build

- name: A-Team Pipeline | webhook | EventListener
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: triggers.tekton.dev/v1alpha1
      kind: EventListener
      metadata:
        name: a-team
        namespace: "{{ ocp_ns }}"
      spec:
        serviceAccountName: pipeline
        triggers:
          - triggerRef: a-team
          - triggerRef: product-build

- name: A-Team Pipeline | webhook | Service
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: v1
      kind: Service
      metadata:
        name: a-team
        namespace: "{{ ocp_ns }}"
      spec:
        ports:
          - name: http-listener
            protocol: TCP
            port: 8080
            targetPort: 8000
        selector:
          eventlistener: a-team

- name: A-Team Pipeline | webhook | Route
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: v1
      kind: Route
      metadata:
        name: a-team
        namespace: "{{ ocp_ns }}"
      spec:
        to:
          kind: Service
          name: a-team
        port:
          targetPort: http-listener
  register: route_response

- name: A-Team Pipeline | k8s | Set hook url
  ansible.builtin.set_fact:
    hook_url: "http://{{ route_response.result.spec.host }}"
