---
- name: A-Team Pipeline | k8s | Validate vars are provided
  ansible.builtin.assert:
    that:
      - vars[item] != None
  with_items:
    - ocp_host
    - ocp_ns
    - ocp_key
    - ocp_pipeline_key
    - ci_repo_url
    - ci_revision
    - ci_repos_endpoint
    - ci_images_endpoint
    - ci_logs_endpoint
    - gitlab_token
    - gitlab_key
    - gitlab_host_url
    - aws_access_key_id
    - aws_secret_access_key
    - aws_region
    - aws_tf_region
    - aws_bucket_repos
    - aws_bucket_logs
    - aws_product_build_bucket_repos
    - stream
    - ssh_private_key
    - tf_os_prefix
    - tf_os_options
    - tf_distro_name
    - tf_format
    - tf_api_key
    - tf_ssh_key
    - ssl_verify

- name: A-Team Pipeline | k8s | Create Tooling Container ImageStream
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: image.openshift.io/v1
      kind: ImageStream
      metadata:
        name: create_pipeline
        namespace: "{{ ocp_ns }}"
        annotations:
          openshift.io/display-name: create_pipeline
      spec:
        tags:
          - name: latest
        lookupPolicy:
          local: true

- name: A-Team Pipeline | k8s | Create Tooling Container BuildConfig
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: build.openshift.io/v1
      kind: BuildConfig
      metadata:
        name: create-pipeline-tooling
        namespace: "{{ ocp_ns }}"
      spec:
        runPolicy: Serial
        triggers:
          - type: "GitLab"
            gitlab:
              secret: "{{ gitlab_token }}"
          - type: "ConfigChange"
        source:
          git:
            uri: "https://gitlab.com/redhat/edge/ci-cd/pipelines-as-code.git"
            ref: main
          contextDir: ./deployment/
        strategy:
          type: Docker
          dockerStrategy:
            env:
              - name: GIT_SSL_NO_VERIFY
                value: "true"
            dockerfilePath: Containerfile
        output:
          to:
            kind: "ImageStreamTag"
            name: "create_pipeline:latest"

- name: A-Team Pipeline | k8s | Create create_yum_repo Container ImageStream
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: image.openshift.io/v1
      kind: ImageStream
      metadata:
        name: create_yum_repo
        namespace: "{{ ocp_ns }}"
        annotations:
          openshift.io/display-name: create_yum_repo
      spec:
        tags:
          - name: latest
        lookupPolicy:
          local: true

- name: A-Team Pipeline | k8s | Create create_yum_repo Container BuildConfig
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: build.openshift.io/v1
      kind: BuildConfig
      metadata:
        name: create-yum-repo-tooling
        namespace: "{{ ocp_ns }}"
      spec:
        resources:
          requests:
            memory: 256Mi
          limits:
            memory: 1Gi
        runPolicy: Serial
        triggers:
          - type: "GitLab"
            gitlab:
              secret: "{{ gitlab_token }}"
          - type: "ConfigChange"
        source:
          git:
            uri: "https://gitlab.com/redhat/edge/ci-cd/pipelines-as-code.git"
            ref: main
          contextDir: ./ci/create_yum_repo
        strategy:
          type: Docker
          dockerStrategy:
            env:
              - name: GIT_SSL_NO_VERIFY
                value: "true"
            dockerfilePath: Containerfile
        output:
          to:
            kind: "ImageStreamTag"
            name: "create_yum_repo:latest"

- name: A-Team Pipeline | k8s | Secret
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: v1
      kind: Secret
      metadata:
        name: secure-props
        namespace: "{{ ocp_ns }}"
      type: Opaque
      data:
        GITLAB_TOKEN: "{{ gitlab_token | b64encode }}"
        GITLAB_KEY: "{{ gitlab_key | b64encode }}"
        AWS_ACCESS_KEY_ID: "{{ aws_access_key_id | b64encode }}"
        AWS_SECRET_ACCESS_KEY: "{{ aws_secret_access_key | b64encode }}"
        SSH_PRIVATE_KEY: "{{ ssh_private_key | b64encode }}"
        TF_API_KEY: "{{ tf_api_key | b64encode }}"
        TF_SSH_KEY: "{{ tf_ssh_key | b64encode }}"

# Create a separate secret for the CA cert to avoid mounting every secret to tekton git-clone tasks
- name: A-Team Pipeline | k8s | CA-cert Secret
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: v1
      kind: Secret
      metadata:
        name: ca-certs
        namespace: "{{ ocp_ns }}"
      type: Opaque
      data:
        ca.crt: "{{ ca_cert | b64encode }}"
        ca.pem: "{{ ca_cert | b64encode }}"
